//
//  DocmailMobileWrapper.swift
//  Fiddling With Postcards
//
//  Created by Will Gunby on 30/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit

class DocmailMobileWrapper: JSONWrapper {
    
    /*    
    var baseurl='https://www.cfhdocmail.com/mobileapiLive/MobileWS.svc';
    var signupurl = 'https://www.cfhdocmail.com/live/signup.aspx?ReferrerAccountNo=ZDMiPhoneApp';
    var loginurl='https://www.cfhdocmail.com/live/login.aspx';
    var topupurl='https://www.cfhdocmail.com/live/topup.aspx';
    */
    
    //check connection?
    //retries?
    
    var callingApp  = ""
    var username    = ""
    var password    = ""
    
    init(forPayPalWithWsResponseDelegate :WebserviceResponse!, callingApp:String, useLive:Bool){
        self.callingApp = callingApp
        
        var liveOrTest:String
        if useLive  { liveOrTest = "Live" }
        else        { liveOrTest = "Test" }
        
        let url = "https://www.cfhdocmail.com/mobileapi\(liveOrTest)/MobileWS.svc"
        super.init(wsResponseDelegate: forPayPalWithWsResponseDelegate, wsUrl: url)
    }
    
    init(forAccountWithUsername:String, password:String, wsResponseDelegate :WebserviceResponse!, callingApp:String, useLive:Bool){
        self.callingApp = callingApp
        self.username = forAccountWithUsername
        self.password = password
        
        var liveOrTest:String
        if useLive  { liveOrTest = "Live" }
        else        { liveOrTest = "Test" }
        
        let url = "https://www.cfhdocmail.com/mobileapi\(liveOrTest)/MobileWS.svc"
        super.init(wsResponseDelegate: wsResponseDelegate, wsUrl: url)
    }
    
    func testLogin(username:String, password:String){
        
    }
    
    func getTerms(){
        let data = [
            "sCallingApp"   : callingApp
        ]
        super.jsonSend("GetTerms", data: data)
    }
    
    func getMessage(){
        let data = [
            "sCallingApp"   : callingApp
        ]
        super.jsonSend("GetMessage", data: data)
    }
    
    func getReferralCode_Account(){
        let data = [
            "sCallingApp"   : callingApp,
            "sUsr"          : username,
            "sPwd"          : password
        ]
        super.jsonSend("GetReferralCode_Account", data: data)
    }
    
    func checkBalance_Account(){        let data = [
            "sCallingApp"   : callingApp,
            "sUsr"          : username,
            "sPwd"          : password
        ]
        super.jsonSend("CheckBalance_Account", data: data)
    }
    /*
        product:
            'pca6r'
            'pca5r'
    */
    func checkPrice_Addresses(product:String, addressesBase64:String, addPayPalCharge:Bool){
        let data = [
            "sCallingApp"       : callingApp,
            "sUsr"              : username,
            "sPwd"              : password,
            "sMobileProduct"    : product,
            "sBase64Addresses"	: addressesBase64,
            "sAddPayPalCharge"  : "\(addPayPalCharge)"
        ]
        super.jsonSend("CheckPrice_Addresses", data: data)
    }
    
    /* <PayPal Calls> */
    func createOrder_PayPal(product:String){
        let data = [
            "sCallingApp"   : callingApp,
            "sMobileProduct": product
        ]
        super.jsonSend("CreateOrder_PayPal", data: data)
    }
    
    func base64EncodeString(string:String) ->String{
        let plainData = (string as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
        return base64EncodeData(plainData)
    }
    
    func base64EncodeData(data:NSData) ->String{
        let base64String = data.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        return base64String
    }
    
    func placeOrder_PayPal(payPalRef:String, product:String, message:String, imageData:NSData, addressesCSV:String, mailingGUID:NSUUID){
        placeOrder_PayPal(payPalRef, product: product, messageBase64: base64EncodeString(message), imageBase64: base64EncodeData(imageData), addressesBase64: base64EncodeString(addressesCSV), mailingGUID: mailingGUID)
    }
    
    func placeOrder_PayPal(payPalRef:String, product:String, messageBase64:String, imageBase64:String, addressesBase64:String, mailingGUID:NSUUID){
        let data = [
            "sCallingApp"     	: callingApp,
            "sPayPalUser"    	: "not_available",
            "sPayPalPaymentRef" : payPalRef,
            "sMobileProduct"   	: product,
            "sBase64Msg"		: messageBase64,
            "sBase64MainImage"	: imageBase64,
            "sBase64Addresses"	: addressesBase64,
            "sMailingGUID"		: "\(mailingGUID.UUIDString)"
        ]
        super.jsonSend("PlaceOrder_PayPal", data: data)
    }
    
    func checkOrderStatus_PayPal(mailingGUID:NSUUID){
        let data = [
            "sCallingApp"   : callingApp,
            "sMailingGUID"	: "\(mailingGUID.UUIDString)"
        ]
        super.jsonSend("CheckOrderStatus_PayPal", data: data)
    }
    /* </PayPal Calls> */
    
    /* <Account Calls> */
    func createOrder_Account(product:String){
        let data = [
            "sCallingApp"   : callingApp,
            "sUsr"          : username,
            "sPwd"          : password,
            "sMobileProduct": product
        ]
        super.jsonSend("CreateOrder_Account", data: data)
    }
    
    func placeOrder_Account(product:String, message:String, imageData:NSData, addressesCSV:String, mailingGUID:NSUUID){
        placeOrder_Account(product, messageBase64: base64EncodeString(message), imageBase64: base64EncodeData(imageData), addressesBase64: base64EncodeString(addressesCSV), mailingGUID: mailingGUID)
    }
    
    func placeOrder_Account(product:String, messageBase64:String, imageBase64:String, addressesBase64:String, mailingGUID:NSUUID){
        let data = [
            "sCallingApp"       : callingApp,
            "sUsr"              : username,
            "sPwd"              : password,
            "sMobileProduct"    : product,
            "sBase64Msg"		: messageBase64,
            "sBase64MainImage"	: imageBase64,
            "sBase64Addresses"	: addressesBase64,
            "sMailingGUID"		: "\(mailingGUID.UUIDString)"
        ]
        super.jsonSend("PlaceOrder_Account", data: data)
    }
    
    func checkOrderStatus_Account(orderRef:String){
        let data = [
            "sCallingApp"   : callingApp,
            "sUsr"          : username,
            "sPwd"          : password,
            "sOrderRef"     : orderRef
        ]
        super.jsonSend("CheckOrderStatus_Account", data: data)
    }
    /* </Account Calls> */
    
    
    
    override func jsonResultProcessor(json:Dictionary<String, AnyObject>) -> Dictionary<String, AnyObject> {
        let dataString = json["d"] as! String
        var result = Dictionary<String, AnyObject>()
        //Docmail mobile has pipe seperated strings within the result
        let kvps = dataString.componentsSeparatedByString("|")
        for kvp in kvps{
            let keyValue = kvp.componentsSeparatedByString(":")
            //add key/value to result
            result[keyValue[0]] = keyValue[1]
        }
        
        return result
    }
    
    
}
