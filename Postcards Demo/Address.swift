//
//  Address.swift
//  PostcardsDemo
//
//  Created by Will Gunby on 10/05/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation
import CoreData

class Address: NSManagedObject {

    @NSManaged var addressLines: String
    @NSManaged var created: NSDate
    @NSManaged var name: String
    @NSManaged var cards: NSSet

}
