//
//  Builder.swift
//  Fiddling With Postcards
//
//  Created by Will Gunby on 04/12/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import Foundation
import UIKit


class VCHome:  VCBase, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate {
    
    @IBOutlet var btnLogout: UIBarButtonItem!
    @IBOutlet var lblUser: UILabel!
    @IBOutlet var vwAction: UIView!
    
    @IBOutlet var imgPicture: UIImageView!
    @IBOutlet var imgArrow: UIImageView!
    @IBOutlet var imgBackground: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        updateUserInfo()
    }
    
    override func viewDidAppear(animated: Bool) {    }
    override func viewWillAppear(animated: Bool) {
        
        imgBackground.gestureRecognizers = nil
        let vwBackTap = UITapGestureRecognizer(target: self, action: "imgBackground_tapped:")
        imgBackground.addGestureRecognizer(vwBackTap)
        
        imgBackground.image = normalizedImage(imgBackground.image!)
    }
    
    func normalizedImage(source:UIImage) ->UIImage {
        if (source.imageOrientation == UIImageOrientation.Up) {return source}
        UIGraphicsBeginImageContextWithOptions(source.size, false, source.scale)
        source.drawInRect(CGRect(origin: CGPoint(x: 0, y: 0), size: source.size))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return normalizedImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func imgBackground_tapped(recognizer:UITapGestureRecognizer){
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("SegueToBuilder_Modal", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //navigationItem.title = ""
    }
    
    @IBAction func btnLogout_up(sender: AnyObject) {
        KeychainService.saveCredentials("", password: "")
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func updateUserInfo(){
        lblUser.text = KeychainService.loadUsername()
    }
    
    
    
}

