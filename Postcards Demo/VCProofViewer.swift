//
//  VCProofViewer.swift
//  PostcardsDemo
//
//  Created by Will Gunby on 06/05/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit


class VCProofViewer:  VCBase, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UIWebViewDelegate, UIDocumentInteractionControllerDelegate , WebserviceResponse {
    
    @IBOutlet var preview: UIWebView!
    
    var card:Card?
    
    @IBOutlet var vwBackgorund: UIView!
    @IBOutlet var lblDismiss: UILabel!
    @IBOutlet var vwInteractionTemplate: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let vwTap = UITapGestureRecognizer(target: self, action: "preview_tapped:")
        preview.addGestureRecognizer(vwTap)
        
        
        //TODO: show details panel?
        
        let html = "<html><head/><body><h1>\(card!.message)</h1></body></html>"
        preview.loadHTMLString(html, baseURL: nil)
        addSwipes()
        loadProofFile()
    }
    
    override func viewDidAppear(animated: Bool) {    }
    override func viewWillAppear(animated: Bool) {
        
    }
    
    func loadProofFile(){
        //card!.resetProofFile()
        if card!.hasProofFile() {
            //println("proofDebug \(card!.proofDebug())")
            btniBooks.hidden = false
            preview.loadRequest(NSURLRequest(URL: self.card!.proofLocalURL()!))
        }
        else {
            btniBooks.hidden = true
            getProofFileFromWeb()
        }
    }
    
    func getProofFileFromWeb(){
        let api = DocmailAPIWrapper(username: KeychainService.loadUsername(), password:KeychainService.loadPassword(), wsResponseDelegate: self, useLive: useLive)
        if let mailingGUID = card!.mailingGUID {
            api.getProofFile(mailingGUID, failureReturnFormat: "JSON")
        }
        else {
            return
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func addSwipes(){
//        setupSwipe(self.view, direction:UISwipeGestureRecognizerDirection.Left, action: "swipe:")
//        setupSwipe(self.view, direction:UISwipeGestureRecognizerDirection.Right, action: "swipe:")
//        setupSwipe(self.view, direction:UISwipeGestureRecognizerDirection.Up, action: "swipe:")
       //// setupSwipe(lblDismiss, direction:UISwipeGestureRecognizerDirection.Down, action: "swipe:")
        
        setupPan(self.view, action: "panGesture:")
    }
    
    func setupSwipe(target:UIView,direction:UISwipeGestureRecognizerDirection, action:Selector){
        let swipe = UISwipeGestureRecognizer(target: self, action: action)
        swipe.direction = direction
        target.addGestureRecognizer(swipe)
        self.preview.scrollView.panGestureRecognizer.requireGestureRecognizerToFail(swipe)
    }
    
    func swipe(gesture:UIGestureRecognizer){
        if let swipGest = gesture as? UISwipeGestureRecognizer {
            print("Swiped \(swipGest.direction)")
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                
            })
        }
    }
    
    
    func setupPan(target:UIView, action:Selector){
        let pan = UIPanGestureRecognizer(target: self, action: action)
        target.addGestureRecognizer(pan)
        pan.cancelsTouchesInView = false
        //self.preview.addGestureRecognizer(pan)
//        self.preview.scrollView.panGestureRecognizer.requireGestureRecognizerToFail(pan)
    }
    
    var startLocation:CGPoint?
    func panGesture(sender:UIPanGestureRecognizer) {
        
        sender.cancelsTouchesInView = false
        print("velocityInView.y: \(sender.velocityInView(self.view).y)")
        
        if (sender.state == UIGestureRecognizerState.Began) {
            startLocation = sender.locationInView(self.view)
        }
        else if (sender.state == UIGestureRecognizerState.Ended) {
            let stopLocation = sender.locationInView(self.view)
            let dx = stopLocation.x - startLocation!.x
            let dy = stopLocation.y - startLocation!.y
            let distance = sqrt(dx*dx + dy*dy)
            print("Distance: \(distance)")
            
            //only trigger dismiss where vertical stroke length is >60px and velocity >500 (px/sec?) - this feels pretty nice/natural.  Originally intended for use with the webview panning, but couldn't get that working (without subclassing views and gesture recognisers) but feels nicer than thebuild in swipe, so kept it!
            if dy > CGFloat(60) {
                if sender.velocityInView(self.view).y > 500{
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
                        
                    })
                }
            }
        }
    }
    
    @IBAction func btnIBooks_up(sender: AnyObject) {
        self.openDocumentIn()
    }
    
    func preview_tapped(recognizer:UITapGestureRecognizer){
        dispatch_async(dispatch_get_main_queue()) {
            /* offer:
                    save to iBooks
                                    http://www.raywenderlich.com/forums/viewtopic.php?f=2&t=2336
                                    http://stackoverflow.com/questions/27959023/swift-how-to-open-local-pdf-from-my-app-to-ibooks
                                    
                    close           (dismis or pop ?)
            */
        }
        
    }
    @IBOutlet var btniBooks: UIButton!
    var docController:UIDocumentInteractionController?
    func openDocumentIn() {
        docController = UIDocumentInteractionController(URL: card!.proofLocalURL()!)
        docController!.name = "Docmail postcard \(card!.orderRef)"
        
        //let view = docController!.delegate?.documentInteractionControllerViewControllerForPreview!(docController!).view!
        
        let url = NSURL(string:"itms-books:");
        if UIApplication.sharedApplication().canOpenURL(url!) {
            docController!.presentOpenInMenuFromRect(CGRectZero, inView: btniBooks, animated: true)
            print("iBooks is installed")
            //TODO: proof has UUID as name in iBooks, not pretty!  Would mean revision of proof storing, or
        }else{
            print("iBooks is not installed")
            alert("iBooks is not installed", message: "Please install iBooks to allow the app to save to your iBooks library")
        }
    }
    
//    func documentInteractionControllerViewForPreview(controller: UIDocumentInteractionController) -> UIView? {
//        // Use the rootViewController here so that the preview is pushed onto the navbar stack
////        var appDelegate = UIApplication.sharedApplication().delegate
////        if let nav = appDelegate!.window!!.rootViewController?.navigationController {
////            return nav.navigationBar
////        }
//
//        var view = UIView()
//        view.tintColor = UIColor.redColor()
//        view.backgroundColor = UIColor.yellowColor()
//        return view
//    }
    
//    doc
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //navigationItem.title = ""
    }
    
    
    func response(sourceName:String, data:Dictionary<String,AnyObject>){
        //TODO: too much logic in the view controller - needs refactoring out
        dispatch_async(dispatch_get_main_queue(), {
            var success = true
            var isSoap = false
            
            if (data["Error code"] != nil){
                success = false
                isSoap = true
            }
            else {
                if let resultCode = data["Result"] as? String {
                    if Int(resultCode) <= 0 {
                        print(data["Msg"]!)
                        success = false
                        isSoap = false
                    }
                }
            }
            
            if !success {
                if !isSoap  {
                    //we're not using the mobile API here
                }
                else        {
                    //data["Error code"] as! String
                    if let errorMsg = data["Error message"] as? String {
                        if errorMsg == "The mailing does not have a proof." {
                            self.alert("Proof not ready", message: "Please check again later" as String)
                        }
                        else {
                            self.alert("Docmail error", message: errorMsg)
                        }
                    }
                }
                
                self.endWait()
            }
            else {
                //TODO: WSWrappers class is untested with GetProofFile & the code therein is a bit hacky...
                if let fileData = data["fileData"] as? NSData{
                    self.card!.storeProofFile(fileData)
                    self.preview.loadRequest(NSURLRequest(URL: self.card!.proofLocalURL()!))
                    self.btniBooks.hidden = false
                }
            }
        })
    }
    
    func progressUp(connection:NSURLConnection, bytesWritten: Int, totalBytesWritten : Int,totalBytesExpectedToWrite: Int){
        
    }
    
    func error(data: NSData!, response: NSURLResponse!, error: NSError!) {
        endWait()
        
        dispatch_async(dispatch_get_main_queue()) {
        
            if error.code == -1005 {
                self.alert("Network error", message: "Connection lost")
            } else {
                self.alert("Network error", message: "data: \(data)\nresponse: \(response)\nerror: \(error)")
        
            }
        }
    }
    
    func notConnected() {
        endWait()
        dispatch_async(dispatch_get_main_queue()) {
            self.alert("Network error", message: "not connected")
        }
    }
    
    
}

