//
//  CardModel.swift
//  Postcards Demo
//
//  Created by Will Gunby on 25/04/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CardInterface<T:Card>:ModelInterface<T> {
    
    init() {
        super.init(entityName: "Card", matchKey: "id")
    }
    
    class func docsPath()->NSURL{
        // documentsDirectory is your app's document path
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let docsDir = paths[0] as String
        return NSURL(fileURLWithPath: docsDir)
    }
    
    private class func initPath(subDir:String)->NSURL{
        let filemgr = NSFileManager.defaultManager()
        let fileDirURL = docsPath().URLByAppendingPathComponent(subDir)        //check/create dir
        let fileDirString = fileDirURL.path!
        var isDir = ObjCBool(true)
        Swift.print("checking dir exists \(fileDirString)")
        if !filemgr.fileExistsAtPath(fileDirString, isDirectory: &isDir ) {
            do {
                Swift.print("... no. creating dir \(fileDirString)")
                try filemgr.createDirectoryAtPath(fileDirString, withIntermediateDirectories: true, attributes: nil)
            } catch {
                let err = error as NSError
                Swift.print("createDirectoryAtPath error: \(err.localizedDescription)")
            }
        }
        return fileDirURL.URLByStandardizingPath!
    }
    
    class func proofsPathString()->String{
        return "\(proofsPath)"
    }
    class func proofsPath()->NSURL{
        return initPath("proofs")
    }
    
    class func imagesPathString()->String{
        return "\(imagesPath)"
    }
    class func imagesPath()->NSURL{
        return initPath("images")
    }
    
    func addCardWithNewImage(message:String, imageData:NSData, addresses: [Address])->Card{
        // try to save file
        let imageUUID = NSUUID().UUIDString
        let imagePathURL = Card.imagePath(imageUUID)
        let imagePathString = imagePathURL!.path!
        
        
        if (!imageData.writeToFile(imagePathString, atomically: true)) {
            Swift.print("writeToFile error")
        }
        
        let fileMgr = NSFileManager.defaultManager()
        if !fileMgr.fileExistsAtPath(imagePathString){print("fail")}

        
        return addCard(message, imageUUID:imageUUID, addresses:addresses)
    }
    
    func addCard(message:String, imageUUID:String, addresses: [Address])->Card{
        let newcard:Card = NSEntityDescription.insertNewObjectForEntityForName("Card", inManagedObjectContext: context!) as! Card
        newcard.message=message
        newcard.imageUUID=imageUUID
        newcard.created=NSDate()
        newcard.objectID
        
        //link addresses to card
        for address in addresses {
            newcard.addresses.setByAddingObject(address)
            address.cards.setByAddingObject(newcard)
        }
        
        save()
        
        return newcard
    }
    
    
    func delete(card:T){
        
        //keep the addresses for now
//        for address in card.addresses {
//            if address.cards.count = 1 { deleteItem(address) }
//        }
        
        //TODO: delete proof file, delete image file!!
        deleteItem(card)
    }
    
    func fetchAll()->[Card]{
        let sortTerm = NSSortDescriptor(key: "created", ascending: false)
        let sort = Array(arrayLiteral: sortTerm)
        
        return fetchAll(sort)
    }
    
}
