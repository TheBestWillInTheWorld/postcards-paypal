
//
//  MatchesTableViewController.swift
//  Tendir
//
//  Created by Will Gunby on 10/01/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit

class VCHistoryTable: UITableViewController  {
    
    var cards = CardInterface()
    var cardStore : [Card] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        cardStore = cards.fetchAll()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //as I can't get async image loading to my satisfaction in the tableview, load all the images up front.
    //te preloading approach does get the images to load, but something still freezes the app and the ipod (possibly image size? )
    var imgFetchCounter = 0
    var images = [String: UIImage]()
    
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return cardStore.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! TableCellBasic

        //TODO: show status, order ref (where available), number of recipients
        print(cardStore)
        print(indexPath.item)
        print(cardStore[indexPath.item])
        
        let card = cardStore[indexPath.item]
        let dateString = card.dateString("dd-MM-yyyy")
        if let orderRef = card.orderRef {
            cell.lblDate?.text = "Order \(orderRef)"
        } else {
            cell.lblDate?.text = dateString
        }
        cell.lblMessage?.text = card.message
        if let imView = cell.imgPreview {
            imView.image = card.image()
        }
        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        /*TODO: show options 
                            [Show detail]               > from here we could send order details via email for Docmail support 
                                                            (ideally another WS call rather than email)
                            [New card to addresses]
                            [New card with image]
                            [New card with message]
                            [Clone as new card]
        */
        //TODO: get and display proof in-app (new VC)
        
        performSegueWithIdentifier("SegueToPreview_New", sender: cardStore[indexPath.row])
        //TODO: enable save to iBooks from app
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            //delete from coredata
            cards.delete(cardStore[indexPath.row])
            cardStore = cards.fetchAll()
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }


    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "SegueToPreview_New" {
            if let vcPreview:VCProofViewer = segue.destinationViewController as? VCProofViewer {
                vcPreview.card = sender as? Card
            }
        }
    }

    

}
