//
//  TableCellBasic.swift
//  PostcardsDemo
//
//  Created by Will Gunby on 17/05/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit


public class TableCellBasic:UITableViewCell{
    
    @IBOutlet var imgPreview: UIImageView!
    @IBOutlet var lblDate   : UILabel!
    @IBOutlet var lblMessage: UILabel!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override public func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
}
