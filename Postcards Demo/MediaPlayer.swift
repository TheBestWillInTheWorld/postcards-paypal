//
//  MediaPlayer.swift
//  PostcardsDemo
//
//  Created by Will Gunby on 05/05/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation
import AVFoundation


public class MediaPlayer{
    
    var arrSounds: [NSURL]!=nil
    var player:AVAudioPlayer? = AVAudioPlayer()
    
    init() {
        //super.init()
        loadSounds()
    }
    
    func playNotification(){
        let uRL = NSBundle.mainBundle().URLForResource("Blop-Mark_DiAngelo", withExtension: "mp3")!
        var error:NSError? = nil
        do {
            player = try AVAudioPlayer(contentsOfURL: uRL)
        } catch let error1 as NSError {
            error = error1
            player = nil
        }
        if let theError = error {
            print(theError)
        }
        player!.play()
    }
    
    
    func playRandom(){
        var index = Int(arc4random() % UInt32(arrSounds.count))
        var uRL = arrSounds[index]
        print("arc4random() % \(arrSounds.count) = \(index) = \(uRL.lastPathComponent)")
        index = Int(arc4random_uniform(UInt32(arrSounds.count)))
        uRL = arrSounds[index]
        print("arc4random_uniform(\(arrSounds.count)) = \(index) = \(uRL.lastPathComponent)")
        do {
            player = try AVAudioPlayer(contentsOfURL: uRL)
        } catch {
            player = nil
        }
        player!.play()
    }
    
    func loadSounds(){
        arrSounds = [NSURL]()
        arrSounds.append(NSBundle.mainBundle().URLForResource("Blop-Mark_DiAngelo", withExtension: "mp3")!)
    }
    
}