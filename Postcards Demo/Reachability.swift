//
//  Reachability.swift
//  Fiddling With Postcards
//
//  Created by Will Gunby on 18/04/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//
import Foundation
import SystemConfiguration

class Reachability {
    
    class func httpNetworkReachable2() -> Bool {
//        let reachability  = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.apple.com").takeRetainedValue()
        let reachability  = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.apple.com")
        //works, but has a long timeout that I don't know how to change

        var flags = SCNetworkReachabilityFlags.ConnectionAutomatic
        if !SCNetworkReachabilityGetFlags(reachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }
    
    class func testNetworkWithHead(timeout:NSTimeInterval, testURL:String, callback:(NSData?, NSURLResponse?, NSError?) -> Void){
        let url = NSURL(string:testURL)
        let request = NSMutableURLRequest(URL: url!)
        
        request.timeoutInterval = timeout
        request.HTTPMethod = "HEAD"
        //init and run HTTP request
        _ = NSURLConnection(request:request,delegate:self,startImmediately:true)
        
        let webClient=NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler:callback )
        
        webClient.resume()
    }
    
    func hostReachable(ipadres:String) -> Bool {
        if let host_name = ipadres.cStringUsingEncoding(NSASCIIStringEncoding) {
            let reachability  = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, host_name)
            var flags = SCNetworkReachabilityFlags.ConnectionAutomatic
            if !SCNetworkReachabilityGetFlags(reachability!, &flags) {
                return false
            }
            let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
            let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
            
            return (isReachable && !needsConnection)
        }else{
            return false
        }
    }
    
}