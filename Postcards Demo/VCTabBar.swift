//
//  VCTabBar.swift
//  PostcardsDemo
//
//  Created by Will Gunby on 04/05/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation
import UIKit

class VCTabBar: UITabBarController, UITabBarControllerDelegate, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(tabBar: UITabBar, didBeginCustomizingItems items: [UITabBarItem]) {
        self.view.tintColor = appThemeColour
        tabBar.tintColor    = appThemeColour
        tabBar.barTintColor = appThemeColour
        //if let items = tabBar.items {
            //for item in items {
                //item.tintColor = UIColor(netHex:0x57AF00)
            //}
        //}
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        navigationController.navigationBar.tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    }
}