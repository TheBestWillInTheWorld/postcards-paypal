//
//  KeychainService.swift
//  PostcardsDemo
//
//  Created by Will Gunby on 26/04/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//
//source http://matthewpalmer.net/blog/2014/06/21/example-ios-keychain-swift-save-query/

import Foundation
import UIKit
import Security

public class KeychainService: NSObject {
    // Identifiers
    static let serviceIdentifier:String = "Docmail"
    static let userAccount      :String = "authenticatedUser"
    static let accessGroup      :String = "Docmail"
    
    // Arguments for the keychain queries
    static let kSecClassValue                  = NSString(format: kSecClass)
    static let kSecAttrAccountValue            = NSString(format: kSecAttrAccount)
    static let kSecValueDataValue              = NSString(format: kSecValueData)
    static let kSecClassGenericPasswordValue   = NSString(format: kSecClassGenericPassword)
    static let kSecAttrServiceValue            = NSString(format: kSecAttrService)
    static let kSecMatchLimitValue             = NSString(format: kSecMatchLimit)
    static let kSecReturnDataValue             = NSString(format: kSecReturnData)
    static let kSecMatchLimitOneValue          = NSString(format: kSecMatchLimitOne)
    
    /**
    * Exposed methods to perform queries.
    * Note: feel free to play around with the arguments
    * for these if you want to be able to customise the
    * service identifier, user accounts, access groups, etc.
    */
    
    //TODO: first attempt with Keychain failed, using NSUserDefaults for now
    public class func saveCredentials(username: String, password:String) {
//        self.save("\(serviceIdentifier)username", data: username)
//        self.save("\(serviceIdentifier)password", data: password)
        NSUserDefaults.standardUserDefaults().setObject(username, forKey: "\(serviceIdentifier)username")
        NSUserDefaults.standardUserDefaults().setObject(password, forKey: "\(serviceIdentifier)password")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    public class func loadUsername()->String{
        //return self.load("\(serviceIdentifier)username") as! String
        let value = NSUserDefaults.standardUserDefaults().objectForKey("\(serviceIdentifier)username") as? String
        if value==nil { return ""}
        return value!
    }
    public class func loadPassword()->String{
        //return self.load("\(serviceIdentifier)password") as! String
        let value = NSUserDefaults.standardUserDefaults().objectForKey("\(serviceIdentifier)password") as? String
        if value==nil { return ""}
        return value!
    }
    
    /**
    * Internal methods for querying the keychain.
    */
    private class func save(service: NSString, data: NSString) {
        let dataFromString: NSData = data.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
        
        // Instantiate a new default keychain query
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
        
        // Delete any existing items
        SecItemDelete(keychainQuery as CFDictionaryRef)
        
        // Add the new keychain item
        SecItemAdd(keychainQuery as CFDictionaryRef, nil)
    }
    
    private class func load(service: NSString) -> NSString? {
        // Instantiate a new default keychain query
        // Tell the query to return a result
        // Limit our results to one item
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, kCFBooleanTrue, kSecMatchLimitOneValue], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue, kSecMatchLimitValue])
        
        let dataPtr  = UnsafeMutablePointer<AnyObject?>()
        
        // Search for the keychain items?
        let status: OSStatus = SecItemCopyMatching(keychainQuery, dataPtr)
        
        var contentsOfKeychain: NSString? = nil
        
        if  status == errSecSuccess {
            let retrievedData = Unmanaged<NSData>.fromOpaque(COpaquePointer(dataPtr)).takeUnretainedValue()
            
            // Convert the data retrieved from the keychain into a string
            contentsOfKeychain = NSString(data: retrievedData, encoding: NSUTF8StringEncoding)
        } else {
            print("Nothing was retrieved from the keychain. Status code \(status)")
        }
        /*
        
        errSecSuccess
        errSecUnimplemented
        errSecParam
        errSecAllocate
        errSecNotAvailable
        errSecAuthFailed
        errSecDuplicateItem
        errSecItemNotFound
        errSecInteractionNotAllowed
        errSecDecode

        */
        
        
        return contentsOfKeychain
    }
}