//
//  Builder.swift
//  Fiddling With Postcards
//
//  Created by Will Gunby on 04/12/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import Foundation
import UIKit

var useLive=true
func testLive()->String{
    if useLive {return "Live"}
    return "Test"
}


class VCBuilder:  VCBase, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, WebserviceResponse {
    
    @IBOutlet var vwContainer: UIView!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var imgPreview: UIImageView!
    @IBOutlet var txtMessage: UITextView!
    @IBOutlet var txtAddress: UITextView!
    @IBOutlet var verticalScrollView: UIScrollView!
    
    @IBOutlet var lblAddImage: UILabel!
    
    let cards = CardInterface()
    var card:Card?
    
    var placeholderColor:UIColor?
    
    
    @IBOutlet var constraintLeading: NSLayoutConstraint!
    var originalConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        placeholderColor = imgPreview.backgroundColor!
        
        let imgTap = UITapGestureRecognizer(target: self, action: "imgPreview_tapped:")
        imgPreview.addGestureRecognizer(imgTap)

        addSwipes()
        
        txtAddress.textContainer.maximumNumberOfLines = 6
        txtAddress.textContainer.lineBreakMode = NSLineBreakMode.ByTruncatingTail
        
        txtMessage.textContainer.maximumNumberOfLines = 15
        txtMessage.textContainer.lineBreakMode = NSLineBreakMode.ByTruncatingTail
        
    }
    
    override func viewDidAppear(animated: Bool) {    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reset(){
        self.imgPreview.backgroundColor = self.placeholderColor
        self.imgPreview.image = nil
        self.txtMessage.text = self.initialMessageText
        self.txtAddress.text = self.initialAddressText
    }
    
    
    func addSwipes(){
        setupSwipe(self.view, direction:UISwipeGestureRecognizerDirection.Left, action: "swipeLeft:")
        setupSwipe(self.view, direction:UISwipeGestureRecognizerDirection.Right, action: "swipeRight:")
    }
    
    func setupSwipe(target:UIView,direction:UISwipeGestureRecognizerDirection, action:Selector){
        let swipe = UISwipeGestureRecognizer(target: self, action: action)
        swipe.direction = direction
        target.addGestureRecognizer(swipe)
        self.vwContainer.addGestureRecognizer(swipe)
    }
    
    func swipeLeft(gesture:UIGestureRecognizer){
        if let swipGest = gesture as? UISwipeGestureRecognizer {
            print("Swiped \(swipGest.direction)")
            self.showImageEntry(true)
        }
    }
    
    func swipeRight(gesture:UIGestureRecognizer){
        if let swipGest = gesture as? UISwipeGestureRecognizer {
            print("Swiped \(swipGest.direction)")
            self.showTextEntry(true)
        }
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func imgPreview_tapped(recognizer:UITapGestureRecognizer){
        btnPickImage_up(imgPreview)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //navigationItem.title = ""
    }
    
    //<text entry>
    //Keyboard delegates
    var notiMan = NotificationManager()
    override func viewWillAppear(animated: Bool) {
        notiMan.registerObserver(UIKeyboardWillShowNotification, block: keyboarWillAppear)
        notiMan.registerObserver(UIKeyboardWillHideNotification, block: keyboarWillDisappear)
        
        if isFreshLoad {
            originalConstraint = NSLayoutConstraint(item: vwContainer, attribute: .Leading, relatedBy: .Equal, toItem: self.view, attribute: .Leading, multiplier: 1.0, constant: 0)
            showTextEntry (false)
            isFreshLoad = false
        }
        
    }
    var isFreshLoad = true
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        notiMan.deregisterAll()
    }
    var keyboardShowing = false
    var kbdHeight:CGFloat!
    func keyboarWillAppear(notification:NSNotification!){
        keyboardShowing = true
        
        let info = notification.userInfo as NSDictionary?
        let rectValue = info![UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let kbSize = rectValue.CGRectValue().size
        
        kbdHeight = kbSize.height
        //verticalScrollView.scrollEnabled = true

    }
    func keyboarWillDisappear(notification:NSNotification!){
        keyboardShowing = false
        verticalScrollView.setContentOffset(CGPointMake(0, 0), animated: true )
        verticalScrollView.scrollEnabled = false
    }
    func textViewDidBeginEditing(textView: UITextView) {
        if kbdHeight == nil {kbdHeight = 0}
        //reset to ensure no double scrollups
        verticalScrollView.setContentOffset(CGPointMake(0, 0), animated: true )
        let gapToBaseline:CGFloat = 72
        let viewport = CGRectMake(0, 0, verticalScrollView.frame.width, verticalScrollView.frame.height + gapToBaseline - kbdHeight)
        //println("\(viewport) - \(textView.frame)")
        if !viewport.contains(textView.frame) && textView == txtMessage {
            
            //in case the scrolling is out, allow the user to scroll the view
            verticalScrollView.scrollEnabled = false
            
            let newYoffset = verticalScrollView.contentOffset.y + (kbdHeight! - gapToBaseline)
            //as this is a static set of content that is smaller than the screen height, we just shift by the KBD height
            verticalScrollView.setContentOffset(CGPointMake(verticalScrollView.contentOffset.x, min(newYoffset,txtAddress.frame.height + 22)), animated: true )
        }
        textView.textColor = UIColor.darkGrayColor()
    }
    
    //handle default/example text in UITextView
    let initialMessageText = "Type a message!"
    let initialAddressText = "Enter postal address"
    func textViewShouldBeginEditing(textView: UITextView) -> Bool{
        var initialText = ""
        
        if      textView == txtMessage  { initialText = initialMessageText }
        else if textView == txtAddress  { initialText = initialAddressText }
        
        if textView.text == initialText {
            textView.text = ""
            textView.textColor = UIColor.darkGrayColor()
        }
        return true
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool{
        var initialText = ""
        
        if      textView == txtMessage  { initialText = initialMessageText }
        else if textView == txtAddress  { initialText = initialAddressText }
        
        if textView.text == "" {
            textView.text = initialText
            textView.textColor = UIColor.darkGrayColor()
        }
        return true
    }
    //</text entry>
    
    func dismissImageSource() {
        if (imageSource != nil) {
            if imageSource!.isViewLoaded() {
                imageSource!.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
    
    //TODO: not currently re-using a created mailing after validation failure = inefficient
    //  slightly tricky to manage this:  if any elements change, we don't have the ability to replace them via this API, so we'd need to abandon that order (ideally cancel it too, but that call isn't available on this API) and start a new order
    var blockThis = false
    func send(){
        if !blockThis  {
            blockThis = true
            self.view.endEditing(true)
            if validateInput() {sendPostcard()}
            blockThis = false
        }
    }
    
    //TODO: check we're not submitting a low res (screen display res) version of the image
    //TODO: consider seperate lines for address entry...?
    
    func validateInput() -> Bool{
        
        //validate inputs are as required
        var validation = [String]()
        
        if imgPreview.image == nil {
            validation.append("A picture")
        }
        if txtAddress.text == initialAddressText {
            validation.append("An address")
        }
        if txtMessage.text == initialMessageText {
            validation.append("A message!")
        }
        if validation.count == 0  { return true }
        
        
        var validationMsg = ""
        for item in validation {
            if validationMsg != "" { validationMsg += "\n" }
            validationMsg += item
        }
        alert("You missed a bit!", message: "Please check you've added\n\n\(validationMsg)")
        
        return false
    }
    
    func sendPostcard(){
        beginWait()
        
        //(this could all be farmed out to the CardEntity class to get it out of the VC)
        let dm = DocmailMobileWrapper(forAccountWithUsername:KeychainService.loadUsername(), password:KeychainService.loadPassword(), wsResponseDelegate: self, callingApp: "Willysoft", useLive:useLive)
        //dm.createOrder_PayPal("pca5r")
        
        //save card and address in CoreData, with image on disk
        let addresses = AddressInterface()
        let nameAddress = getNamePlusAddressCSV()
        let address = addresses.addAddress(nameAddress[0], addressLines: nameAddress[1])
        card = cards.addCardWithNewImage(txtMessage.text, imageData: UIImagePNGRepresentation(self.imgPreview.image!)!, addresses: [address])
        
        //send order to Docmail
        dm.createOrder_Account("pca5r")
    }
    
    @IBAction func btnPeople_up(sender: AnyObject) {
        //close keyboard
        self.view.endEditing(true)
    }
    
    var imageSource : UIAlertController?
    @IBAction func btnPickImage_up(sender: AnyObject) {
        
        imageSource = UIAlertController(title: "Hey!", message: "Where do you want to source the image?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        imageSource!.view.tintColor = appThemeColour

        //only offer camera if it's available on the device
        if  UIImagePickerController.isSourceTypeAvailable(.Camera) {
            imageSource!.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { action in
                self.beginWait()
                self.pickImage(UIImagePickerControllerSourceType.Camera)
                self.imageSource!.dismissViewControllerAnimated(true, completion: nil)
            }))
        }
        imageSource!.addAction(UIAlertAction(title: "Photos", style: .Default, handler: { action in
            self.beginWait()
            self.pickImage(UIImagePickerControllerSourceType.PhotoLibrary)
            self.imageSource!.dismissViewControllerAnimated(true, completion: nil)
        }))
        //TODO: other image sources e.g. WikiMedia, Splashbase, Facebook, Instagram etc.
        
        if imageSource!.actions.count>1{
            imageSource!.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
                self.imageSource!.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(imageSource!, animated: true, completion: nil)
        }
        else {
            imageSource = nil
            //we always offer photo's, so if there's only one action, it is photos - got straight there
            self.beginWait()
            self.pickImage(UIImagePickerControllerSourceType.PhotoLibrary)
        }
    }
    
    func pickImage(source:UIImagePickerControllerSourceType){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = source
        picker.allowsEditing = false
        self.presentViewController(picker, animated: true, completion: {self.endWait()})
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imgPreview.image = normalizedImage(image)
        //remove the placeholder background once we've selected an image
        imgPreview.backgroundColor = UIColor.clearColor()
        lblAddImage.hidden = true
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func normalizedImage(source:UIImage) ->UIImage {
        if (source.imageOrientation == UIImageOrientation.Up) {return source}
        UIGraphicsBeginImageContextWithOptions(source.size, false, source.scale)
        source.drawInRect(CGRect(origin: CGPoint(x: 0, y: 0), size: source.size))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return normalizedImage
    }
    
    //    @IBAction func returnOther(segue:UIStoryboardSegue){}
    
    
    func imageIsShowing()->Bool{
        return getBuilderState()
    }
    
    
    func origin()->CGPoint{
        //return CGPoint(x: 340, y: vwContainer.center.y)
        return CGPoint(x: (vwContainer.bounds.width/2), y: vwContainer.center.y)
    }
    
    var constraintImageShown:NSLayoutConstraint?
    
    
    func setupImageLayout(){
        
        if constraintImageShown == nil {
            constraintImageShown = NSLayoutConstraint(item: vwContainer, attribute: .Leading, relatedBy: .Equal, toItem: self.view, attribute: .Leading, multiplier: 1.0, constant: -self.view.frame.width)
        }
        
        if let constraint = self.originalConstraint {
            self.view.removeConstraint(constraint)
        }
        self.view.addConstraint(self.constraintImageShown!)
        self.view.layoutIfNeeded()
        
        self.constraintLeading = self.constraintImageShown
    }
    
    
    func showImageEntry(animated:Bool){
        
        dispatch_async(dispatch_get_main_queue()) {
            self.btnCancel.setTitle("Edit Message", forState: UIControlState.Normal)
            self.btnNext.setTitle("Send Card!", forState: UIControlState.Normal)
        }

        
        if animated {
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                    self.setupImageLayout()
                }) { (arg:Bool) -> Void in
                    setBuilderState(true)
                    print(self.vwContainer.center)
            }
        } else {
            self.setupImageLayout()
            setBuilderState(true)
            print(self.vwContainer.center)
        }
    }
    
    
    func setupTextLayout(){
        if let constraint = self.constraintImageShown {
            self.view.removeConstraint(constraint)
        }
        self.view.addConstraint(self.originalConstraint)
        self.view.layoutIfNeeded()
        
        self.constraintLeading = self.originalConstraint
    }
    
    
    func showTextEntry(animated:Bool){
        dispatch_async(dispatch_get_main_queue()) {
            self.btnCancel.setTitle("Cancel (discard)", forState: UIControlState.Normal)
            self.btnNext.setTitle("Add Image", forState: UIControlState.Normal)
        }
        
        if animated {
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                    self.setupTextLayout()
                }) { (arg:Bool) -> Void in
                    setBuilderState(false)
                    print(self.vwContainer.center)
                }
        } else {
            self.setupTextLayout()
            setBuilderState(false)
            print(self.vwContainer.center)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func btnCancel_up(sender: AnyObject) {
        if !imageIsShowing() { self.goBack()        }
        else                 {  showTextEntry(true) }
    }
    
    @IBAction func btnNext_up(sender: AnyObject) {
        if !imageIsShowing() { showImageEntry(true) }
        else                 { send()               }
    }
    
    func beginAsyncWait(callName:String){
        beginWait()
    }
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?){
        endWait()
        //the only object add we have is posting an image, so we assume that here (unsafe)
        if callName == "addObjectWithValues" {
            alert("Yay!", message: "image now shared", buttonText: "Ok", closure: { action in
                self.reset()
            })
        }
    }
    func endAsyncWaitWithError(callName:String, error: NSError){
        endWait()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        
        let errortext = error.userInfo["error"] as! String
        alert("Oh no!", message: errortext, buttonText: "Cancel")
    }
    
    //API response stuff
    
    func getAddressCSV() -> String{
        let lines = self.txtAddress.text.componentsSeparatedByString("\n")
        var result:String?
        for line in lines {
            let finalLine = line.replace("\"", with: "'") //hack any quotes to apostrophes to ensure values can be wrapped in qoutes safely
            if result != nil{
                result! += ",\"" + finalLine + "\""
            }
            else {
                result = "\"" + finalLine + "\""
            }
        }
        if result == nil {result = ""}
        return result!
    }
    func getNamePlusAddressCSV() -> [String]{
        let lines = self.txtAddress.text.componentsSeparatedByString("\n")
        var address:String?
        var name:String?
        var isFirst = true
        
        for line in lines {
            let finalLine = line.replace("\"", with: "'") //hack any quotes to apostrophes to ensure values can be wrapped in qoutes safely
            if isFirst {
                name = finalLine
                isFirst = false
            }
            else {
                if address != nil{
                    address! += ",\"" + finalLine + "\""
                }
                else {
                    address = "\"" + finalLine + "\""
                }
            }
        }
        if address == nil {address = ""}
        return [String](arrayLiteral: name!,address!)
    }
    
    func response(sourceName:String, data:Dictionary<String,AnyObject>){
        //TODO: too much logic in the view controller - needs refactoring out
        dispatch_async(dispatch_get_main_queue(), {
            var success = true
            var isSoap = false
            
            if (data["Error code"] != nil){
                success = false
                isSoap = true
            }
            else {
                if let resultCode = data["Result"] as? String {
                    if Int(resultCode) <= 0 {
                        print(data["Msg"]!)
                        success = false
                        isSoap = false
                    }
                }
            }
            
            if !success {
                if !isSoap  {
                    self.alert("Docmail wants more!", message: data["Msg"] as! String)
                }
                else        {
                    //data["Error code"] as! String
                    self.alert("sourceName:\(sourceName)", message: "Data:\(data)" as String)
                }
                
                self.endWait()
            }
            else{
                if sourceName == "GetBalance" || sourceName == "//www.cfhdocmail.com/mobileapi\(testLive())/MobileWS.svc/CheckBalance_Account"	{
                    print(data)
                    if let _ = data["Current balance"] as! String?{
                        isSoap = true
                    }
                    else if let _ = data["Balance"] as! String?{
                        isSoap = false
                    }
                }
                else if sourceName.lowercaseString == "//www.cfhdocmail.com/mobileapi\(testLive())/MobileWS.svc/createOrder_Account".lowercaseString{
                    let dm = DocmailMobileWrapper(forAccountWithUsername:KeychainService.loadUsername(), password:KeychainService.loadPassword(), wsResponseDelegate: self, callingApp: "willy", useLive:useLive)
                    
                    //save additional destails on card
                    if let currentCard = self.card {
                        if let mailingGUID = data["MailingGUID"] as? String {currentCard.mailingGUID = mailingGUID}
                        if let orderRef = data["OrderRef"] as? String       {currentCard.orderRef = orderRef      }
                        self.cards.save()
                    }
                    
                    let mailingGUID = NSUUID(UUIDString: data["MailingGUID"] as! String)
                    //dm.createOrder_PayPal("pca5r")
                    dm.placeOrder_Account("pca5r", message: self.txtMessage.text, imageData: UIImagePNGRepresentation(self.imgPreview.image!)! , addressesCSV: self.getAddressCSV(), mailingGUID: mailingGUID!)
                }
                else if sourceName.lowercaseString == "//www.cfhdocmail.com/mobileapi\(testLive())/MobileWS.svc/placeOrder_Account".lowercaseString{
                    self.reset()
                    self.endWait()
                    player.playNotification()
                    self.alert("Card sent!", message: "(they'll love it!)", buttonText: "", closure: { (action:UIAlertAction!) -> Void in
                        
                    })
                    self.goBack()
                }
            }
        })
    }
    
    func progressUp(connection:NSURLConnection, bytesWritten: Int, totalBytesWritten : Int,totalBytesExpectedToWrite: Int){
        
    }
    
    func error(data: NSData!, response: NSURLResponse!, error: NSError!) {
        endWait()
        
        dispatch_async(dispatch_get_main_queue()) {
            self.alert("Network error", message: "data: \(data)\nresponse: \(response)\nerror: \(error)")
        }
    }
    
    func notConnected() {
        endWait()
        dispatch_async(dispatch_get_main_queue()) {
            self.alert("Network error", message: "not connected")
        }
    }
    
    
    func goBack(){
        showTextEntry(true)
        //double dismiss as I cant get the main form dismissing from the alert properly
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        })
    }
    
    
}

