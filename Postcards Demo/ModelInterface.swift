//
//  ModelInterface.swift
//  PostcardsDemo
//
//  Created by Will Gunby on 26/04/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation
import CoreData
import UIKit


public class ModelInterface<T:NSManagedObject>:NSObject{
    
    var delegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var context : NSManagedObjectContext?
    
    var entityName:String
    var matchKey:String
    
    init(entityName:String, matchKey:String) {
        self.context = delegate.managedObjectContext!
        self.entityName = entityName
        self.matchKey = matchKey
    }
    
    
    
    public func itemExists(matchValue:String) -> Bool{
        let request = NSFetchRequest(entityName: entityName)
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "\(matchKey) = %@", matchValue)
        
        var error:NSError? = nil
        var results: [AnyObject]?
        do {
            results = try context!.executeFetchRequest(request)
        } catch let error1 as NSError {
            error = error1
            results = nil
        }
        if error != nil {Swift.print("\(error)")}
        
        return results?.count > 0
    }
    
    public func print(item:AnyObject){
        Swift.print(item)
    }
    
    
    
    func fetchAll(sort:Array<NSSortDescriptor>)->[T]{
        let request = NSFetchRequest(entityName: entityName)
        request.returnsObjectsAsFaults = false
        request.sortDescriptors = sort
        
        var error:NSError? = nil
        var results: [AnyObject]?
        do {
            results = try context!.executeFetchRequest(request)
        } catch let error1 as NSError {
            error = error1
            results = nil
        }
        if error != nil {Swift.print("\(error)")}
        
        return results as! [T]
    }
    
    
    func fetchWithKey(matchName:String) ->T?{
        let request = NSFetchRequest(entityName: entityName)
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "\(matchKey) = %@", matchName)
        
        var error:NSError? = nil
        var results: [AnyObject]?
        do {
            results = try context!.executeFetchRequest(request)
        } catch let error1 as NSError {
            error = error1
            results = nil
        }
        if error != nil {Swift.print("\(error)")}
        
        if  (results==nil      ) { Swift.print("\(entityName) for \(matchKey) \(matchName) not found"); return nil}
        if !(results?.count > 0) { Swift.print("\(entityName) for \(matchKey) \(matchName) not found"); return nil}
        if  (results?.count > 1) { Swift.print("Too many \(entityName) matches for \(matchKey) \(matchName)"); return nil}
        
        return results?.first as? T
        //call not async
        //        for result: AnyObject in results!{
        //            closureOnFound(result as! T,closureArg)
        //        }
    }
    
    
    func deleteItem(matchMessage:String){
        let item = fetchWithKey(matchMessage)
        if item != nil { deleteItem(item!) }
    }
    
    func deleteItem(item:T){
        Swift.print("deleting \(entityName) \(item)")
        context?.deleteObject(item)
        save()
    }
    
    func save(){
        var error:NSError? = nil
        do {
            try context!.save()
        } catch let error1 as NSError {
            error = error1
        }
        if error != nil {Swift.print("\(error)")}
    }
    
    
}