

(groundwork laid for multiple addresses per card in Codedata, but will need adjustments in the Docmail interface)

TODO
----

* create new composer view controller
//* accept and store credentials - force login on load.  enable log out.
* update history view controller: custom item views - possibly a preview view controller
* warnings on unsent/pending orders - it already saves cards before network/send, so switching to sending from store is what's needed.  Need a package/unpackage system to get to/from Docmail-able objects - suggest the refactor item below will help with this.
* also needs to know the current card object when send has failed - probably offer a save & send later
* refactor - especially getting some of the Docmail functionality out of the view controller (probably to be driven from the CardsInterface class)

extras
------
* switch credentials store to keychain (https://github.com/matthewpalmer/Locksmith ?) 
* copy elements from library to new card
* access device contacts as address source
* optionally save address to device contacts
* app etension to enable image sraing into the app, and if possible an action on a contact to send a postcard.

* Background worker for status polling
* Background worker for retrying/sending stuck cards
* Save cards to cloud (for upgrade/device transfer)
* Swipe to flip card
* Size/delivery options
* Proper landscape display
* Large screen layouts

* PayPal via legacy MPL






iOS postcards (more readable in gmail)

login >---¬
          |
          V



[create card from here?]





 [create][sent][logout]
   |       |
   |        --------¬
   V                |

------------------

     [image]

------------------

    [message]

------------------

 [single address]

------------------

                    |
--------------------
|
V

Sent Cards
[image]
[date]
[name]
[status]
…

> drill down for details
> copy:
Ø  Address

Ø  Image

Ø  Message

Ø  Whole card

> delete
