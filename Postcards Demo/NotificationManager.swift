//
//  NotificationManager.swift
//  ChatSnap
//
//  Created by Will Gunby on 08/02/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation


//source: http://moreindirection.blogspot.co.uk/2014/08/nsnotificationcenter-swift-and-blocks.html
class NotificationManager {
    private var observerTokens: [AnyObject] = []
    
    deinit {
        deregisterAll()
    }
    
    func deregisterAll() {
        for token in observerTokens {
            NSNotificationCenter.defaultCenter().removeObserver(token)
        }
        
        observerTokens = []
    }
    
    func registerObserver(name: String!, block: (NSNotification! -> Void)) {
        let newToken = NSNotificationCenter.defaultCenter().addObserverForName(name, object: nil, queue: nil) {note in
            block(note)
        }
        
        observerTokens.append(newToken)
    }
    
    func registerObserver(name: String!, forObject object: AnyObject!, block: (NSNotification! -> Void)) {
        let newToken = NSNotificationCenter.defaultCenter().addObserverForName(name, object: object, queue: nil) {note in
            block(note)
        }
        
        observerTokens.append(newToken)
    }
}