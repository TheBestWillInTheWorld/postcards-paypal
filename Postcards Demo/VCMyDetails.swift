//
//  ViewController.swift
//  Fiddling With Postcards
//
//  Created by Will Gunby on 28/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit

class VCMyDetails: VCBase, WebserviceResponse{
    
    
    //could extend this to an auto-balance checker that runs in background periodically checking balance and warning the user.
    //      ideally it would just use push from a server app, but I'm not making that right now (+ should probably be build in to Docmail!)
    
    //I haven't used the mobileAPI here (which was supposed to be the point!).
    //  I'll need to do that to get the PayPal side working.
    //      could hotwire it for allowing topups fromPayPal!!
    
    func response(sourceName:String, data:Dictionary<String,AnyObject>){
        dispatch_async(dispatch_get_main_queue(), {
            var success = true
            var isSoap = false
            
            if (data["Error code"] != nil){
                success = false
                isSoap = true
            }
            else {
                if let resultCode = data["Result"] as? String {
                    if Int(resultCode) <= 0 {
                        print(data["Msg"]!)
                        success = false
                        isSoap = false
                    }
                }
            }
            
            if !success {
                if !isSoap  { self.txtResult.text = data["Msg"] as! String }
                else        { self.txtResult.text = "sourceName:\(sourceName)\nData:\n\(data)" }
            }
            else{
                if sourceName == "GetBalance" || sourceName == "//www.cfhdocmail.com/mobileapi\(testLive())/MobileWS.svc/CheckBalance_Account"	{
                    print(data)
                    if let balance = data["Current balance"] as! String?{
                        self.lblBalance.text = "£\(balance)"
                        isSoap = true
                    }
                    else if let balance = data["Balance"] as! String?{
                        self.lblBalance.text = "£\(balance)"
                        isSoap = false
                    }
                    self.txtResult.text = ""
                }
                else {
                    self.txtResult.text = "\(data)"
                }
            }
        })
    }
    
    func progressUp(connection:NSURLConnection, bytesWritten: Int, totalBytesWritten : Int,totalBytesExpectedToWrite: Int){
        
    }
    
    func error(data: NSData!, response: NSURLResponse!, error: NSError!) {
        endWait()
        self.txtResult.text = "data: \(data)\nresponse: \(response)\nerror: \(error)"
    }
    func notConnected() {
        endWait()
        self.txtResult.text = "not connected"
    }

    
    @IBOutlet var txtUsername: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtResult: UITextView!
    @IBOutlet var lblBalance: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCreateACard_up(sender: AnyObject) {
        //self.performSegueWithIdentifier("CreateACard", sender: self)
    }
    
    @IBAction func btnJSON_up(sender: AnyObject) {
        let dm = DocmailMobileWrapper(forAccountWithUsername:txtUsername.text!, password:txtPassword.text!, wsResponseDelegate: self, callingApp: "willy", useLive:useLive)
        dm.checkBalance_Account()
    }
    
    @IBAction func btnSOAP_up(sender: AnyObject) {
        let dm = DocmailAPIWrapper(username:txtUsername.text!, password:txtPassword.text!, wsResponseDelegate: self, useLive:useLive)
        dm.getBalance(nil ,returnFormat:"JSON")
    }

    
    @IBAction func btnClear_up(sender: AnyObject) {
        txtResult.text = ""
        lblBalance.text = "?"
    }
    
    
    @IBAction func btnCreateMailing_up(sender: AnyObject) {
        let dm = DocmailMobileWrapper(forAccountWithUsername:txtUsername.text!, password:txtPassword.text!, wsResponseDelegate: self, callingApp: "willy", useLive:useLive)
        //dm.createOrder_PayPal("pca5r")
        dm.createOrder_Account("pca5r")
    }
    
    @IBOutlet var btnLive: UIButton!
    @IBOutlet var btnTest: UIButton!
    
    @IBAction func btnLive_down(sender: AnyObject) {
        if !useLive {
            useLive = !useLive
            toggleButtons(btnLive, btnOff: btnTest)
        }
    }
    
    func toggleButtons(btnOn:UIButton, btnOff:UIButton){
        let offBackColour = btnOn.backgroundColor
        let offTextColour = btnOn.titleColorForState(.Normal)
            
        btnOn.backgroundColor = btnOff.backgroundColor
        btnOn.setTitleColor(btnOff.titleColorForState(.Normal), forState: .Normal)
        btnOff.backgroundColor = offBackColour
        btnOff.setTitleColor(offTextColour, forState: .Normal)
    }

    @IBAction func btnTest_down(sender: AnyObject) {
        if useLive {
            useLive = !useLive
            toggleButtons(btnLive, btnOff: btnTest)
        }
    }
    
    @IBAction func btnListImages(sender: AnyObject) {
        
        let imagesDir = CardInterface.imagesPathString()
        
        //check/create images dir
        let filemgr = NSFileManager.defaultManager()
        let files: [AnyObject]?
        do {
            files = try filemgr.contentsOfDirectoryAtPath(imagesDir)
        } catch  {
            files = nil
        }

        txtResult.text = "\(files!)"
//        for file in files
//            txtResult.text.append(file.name)
//        }
    }
    
    
    
}

