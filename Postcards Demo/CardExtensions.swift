//
//  CardExtensions.swift
//  PostcardsDemo
//
//  Created by Will Gunby on 26/04/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation
import UIKit

//keep custom methods out of the xCode generated files
extension Card{
    
    class func imagePath(imageUUID:String)->NSURL?{
        let imagesDir = CardInterface.imagesPath()
        let imagePath = imagesDir.URLByAppendingPathComponent(imageUUID).URLByAppendingPathExtension("png")
        return imagePath
        
    }
    
    func imagePathString()->String?{
        let path = imagePath()
        if path == nil {return nil}
        return path!.path
    }
    func imagePath()->NSURL?{
        return Card.imagePath(imageUUID)
    }
    
    func image()->UIImage?{
        let path = imagePath()
        print(path)
        if path == nil {return nil}
        let fileMgr = NSFileManager.defaultManager()
        let pathString = path!.path
        if !fileMgr.fileExistsAtPath(pathString!){
            print ("image not found at: \(pathString)")

            return nil
        }
        
        return UIImage(contentsOfFile: pathString!)!
    }
    
    func dateString(format:String)->String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone()
        return dateFormatter.stringFromDate(created as NSDate)
    }
    
    
        
    func proofPathString()->String?{
        let path = proofPath()
        if path == nil {return nil}
        return path!.path
    }
    func proofPath()->NSURL?{
        let proofsDir = CardInterface.proofsPath()
        let proofPath = proofsDir.URLByAppendingPathComponent(proofPdfUUID!).URLByAppendingPathExtension("pdf")
        print ("proofPath: \(proofPath)")
        return proofPath
    }
    
    func proofLocalURL()->NSURL?{
        let path:NSURL = proofPath()!
        if !Card.fileExistsAtPath(path) {return nil}
        return proofPath()
    }
    
    private class func fileExistsAtPath(url:String?)->Bool{
        if url == nil {return false}
        return fileExistsAtPath(NSURL(fileURLWithPath:url!))
    }
    
    private class func fileExistsAtPath(url:NSURL?)->Bool{
        if url == nil {return false}
        let fileMgr = NSFileManager.defaultManager()
        let pathString = url!.path
        return fileMgr.fileExistsAtPath(pathString!)
    }
    
    func proofDebug()->NSString?{
        let path = proofPathString()
        print(path)
        if !Card.fileExistsAtPath(path) {return nil}
        
        let fileData = NSData.init(contentsOfFile: path!)
        
        if let datastring = NSString(data: fileData!, encoding:NSUTF8StringEncoding) {
            return datastring
        }
        else if let datastring = NSString(data: fileData!, encoding: NSASCIIStringEncoding) {
            return datastring
        }
        return nil
    }
    
    //func fetchProof(){
    //TODO: move the WS proof fetch into here with the local vs. remote fetching logic
    //}
    
    func hasProofFile()->Bool{
        if proofPdfUUID == nil {return false}
        return Card.fileExistsAtPath(proofPath()!)
    }
    
    func storeProofFile(pdfData:NSData) {
        //let error: NSError?
        if proofPdfUUID == nil {
            proofPdfUUID = NSUUID().UUIDString
            let cards = CardInterface()
            cards.save()
        }
        if let pathString = proofPathString() {
            if (!pdfData.writeToFile(pathString, atomically: true)) {
                print("writeToFile error")
            }
        }
        
    }
    
    func resetProofFile() {
        if proofPdfUUID != nil {
            let fileMgr = NSFileManager.defaultManager()
            do {
                try fileMgr.removeItemAtPath(proofPathString()!)
                proofPdfUUID = NSUUID().UUIDString
                let cards = CardInterface()
                cards.save()
            } catch {
                print("removeItemAtPath error: \(error)")
            }
        }
    }
    
    func save(){        
        let cards = CardInterface()
        cards.save()
    }
    
}