//
//  ViewController.swift
//  Postcards Demo
//
//  Created by Will Gunby on 25/04/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit

class VCLogin: VCBase, WebserviceResponse {

    @IBOutlet var txtUsername: UITextField!
    @IBOutlet var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.   
    }
    
    override func viewWillAppear(animated:Bool){
        super.viewWillAppear(animated)
        txtUsername.text=""
        txtPassword.text=""
        if KeychainService.loadUsername() != "" && KeychainService.loadPassword() != "" {
            txtUsername.text=KeychainService.loadUsername()
            txtPassword.text=KeychainService.loadPassword()
            self.login(KeychainService.loadUsername(),password: KeychainService.loadPassword())
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func btnLogin(sender: AnyObject) {
        if !validateLogin() {return}
        login(txtUsername.text!, password: txtPassword.text!)
    }
    
    @IBAction func btnSkip(sender: AnyObject) {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("SegueToBuilder", sender: self)
        }
        
    }
    
    func login(username:String, password:String){
        let docmail = DocmailMobileWrapper(forAccountWithUsername: username, password: password, wsResponseDelegate: self, callingApp: "Willysoft", useLive: true)
        beginWait()
        docmail.checkBalance_Account()
        self.view.endEditing(true)
    }
    
    func validateLogin()->Bool{
        var msg = ""
        var result=true
        if txtUsername.text == "" {
            msg = "Please enter a username"
            result = false
        }
        if txtPassword.text == "" {
            if msg != "" {msg += "\n"}
            msg = "Please enter a password"
            result = false
        }
        if !result {alert("You forgot something!", message: msg)}
        return result
    }
    
       
    
    func response(sourceName:String, data:Dictionary<String,AnyObject>){
        self.endWait()
        var success = true
        if let resultCode = data["Result"] as? String {
            if Int(resultCode) <= 0 {
                print(data["Msg"]!)
                success = false
            }
        }
        
        if !success {
            self.alert("Docmail didn't like that :(", message: data["Msg"] as! String)
            KeychainService.saveCredentials("", password: "")
        }
        else {
            //if it succeeded save details and move on
            KeychainService.saveCredentials(txtUsername.text!, password: txtPassword.text!)
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier("SegueToBuilder", sender: self)
            }
        }
        
    }
    
    func progressUp(connection:NSURLConnection, bytesWritten: Int, totalBytesWritten : Int,totalBytesExpectedToWrite: Int){
        
    }
    
    func error(data: NSData!, response: NSURLResponse!, error: NSError!) {
        endWait()
        
        dispatch_async(dispatch_get_main_queue()) {
            self.alert("Network error", message: "data: \(data)\nresponse: \(response)\nerror: \(error)")
        }
    }
    
    func notConnected() {
        endWait()
        dispatch_async(dispatch_get_main_queue()) {
            self.alert("Network error", message: "not connected")
        }
    }
}

