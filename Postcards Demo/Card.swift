//
//  Card.swift
//  PostcardsDemo
//
//  Created by Will Gunby on 10/05/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation
import CoreData

class Card: NSManagedObject {

    @NSManaged var created: NSDate
    @NSManaged var imageUUID: String
    @NSManaged var message: String
    @NSManaged var status: String?
    @NSManaged var orderRef: String?
    @NSManaged var mailingGUID: String?
    @NSManaged var proofPdfUUID: String?
    @NSManaged var addresses: NSSet

}
