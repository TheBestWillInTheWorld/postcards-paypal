//
//  WSWrappers.swift
//  Fiddling With Postcards
//
//  Created by Will Gunby on 30/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import Foundation

//still not sure how to handle multiple requests as they might interfere with each other in both response methods and
protocol WebserviceResponse{
    func response(sourceName:String, data:Dictionary<String,AnyObject>)
    func progressUp(connection:NSURLConnection, bytesWritten: Int, totalBytesWritten : Int,totalBytesExpectedToWrite: Int)
    func error(data:NSData!, response:NSURLResponse!, error:NSError!)
    func notConnected()
    //TODO: docmail validation errors should bubble up here
}


class SOAPWrapper: NSObject, NSURLConnectionDataDelegate, NSXMLParserDelegate {
    //generate SOAP envelope templates for each method here http://www.soapclient.com/soapclient
    //https://www.cfhdocmail.com/TestAPI2/DMWS.asmx
    
    var wsUrl = ""
    var wsNameSpace = ""
    
    let bodyToken = "@BODY@"
    let fieldNameToken = "@FIELDNAME@"
    let fieldValueToken = "@FIELDVALUE@"
    
    var soapEnv = ""
    var fieldPackage = ""
    
    var wsResponseDelegate :WebserviceResponse
    
    let connectionCheckAddress = "https://www.cfhdocmail.com"
    
    init(wsResponseDelegate :WebserviceResponse!,wsUrl:String, wsNameSpace:String){
        self.wsUrl = wsUrl
        self.wsNameSpace = wsNameSpace
        self.wsResponseDelegate = wsResponseDelegate
        soapEnv = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:tm=\"http://microsoft.com/wsdl/mime/textMatching/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:mime=\"http://schemas.xmlsoap.org/wsdl/mime/\" xmlns:tns=\"\(wsNameSpace)\" xmlns:s1=\"http://microsoft.com/wsdl/types/\" xmlns:s=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://schemas.xmlsoap.org/wsdl/soap12/\" xmlns:http=\"http://schemas.xmlsoap.org/wsdl/http/\" xmlns:wsdl=\"http://schemas.xmlsoap.org/wsdl/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" ><SOAP-ENV:Body> \(bodyToken) </SOAP-ENV:Body></SOAP-ENV:Envelope>"
        fieldPackage = "<tns:\(fieldNameToken)>\(fieldValueToken)</tns:\(fieldNameToken)>"
    }
    
    func contentForMethod(methodName:String, fieldValues:Dictionary<String,AnyObject?>) -> String{
        //TODO: XML encode to prevent injection
        var fields = ""
        for field in fieldValues.keys{
            if fieldValues[field]! != nil{
                //only submit non nil fields
                var xmlSnippet = fieldPackage.replace(fieldNameToken, with: field)
                xmlSnippet = xmlSnippet.replace(fieldValueToken, with: "\(fieldValues[field]!!)")
                fields += xmlSnippet
            }
        }
        return "<tns:\(methodName) xmlns:tns=\"\(wsNameSpace)\">\(fields)</tns:\(methodName)>"
    }
    
    func soapSend(methodName:String, content:String){
        
        Reachability.testNetworkWithHead(networkTimeoutSecs, testURL: connectionCheckAddress) { (headData:NSData?, headResponse:NSURLResponse?, headError:NSError?) -> Void in
            
            //if there was an error show as not connected (not quite correct as we don't pass the error back up the chain...)
            if (headError != nil) {
                self.wsResponseDelegate.notConnected()
                return
            }
            
            let message = self.soapEnv.replace(self.bodyToken, with: content)
            let url = NSURL(string:self.wsUrl)!
            let request = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: networkTimeoutSecs)
            let msgLength = String(message.characters.count)
            
            request.addValue("text/xml; charset=utf-8",             forHTTPHeaderField: "Content-Type")
            request.addValue(msgLength,                             forHTTPHeaderField: "Content-Length")
            request.addValue(self.wsUrl,                            forHTTPHeaderField: "url")
            request.addValue(methodName,                            forHTTPHeaderField: "method")
            request.addValue("\"\(self.wsNameSpace)\(methodName)\"",forHTTPHeaderField: "SOAPAction")
            
            request.HTTPMethod = "POST"
            request.HTTPBody = message.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            //init and run HTTP request
            
            print(self.wsUrl)
            print(request)
           
            
            let webClient=NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: self.gotResponse)
            webClient.resume()
            
        }
    }
    func gotResponse(data:NSData?, response:NSURLResponse?, error:NSError?){
        if error != nil{
            print("error: \(error)")
            wsResponseDelegate.error(data, response:response, error:error)
        }
        else {
            //XML deserialise  http://dubinski.org/wpis/easy-xml-parsing-in-swift/
            print("gotResponse")
            //var datastring = NSString(data: data, encoding: NSUTF8StringEncoding)
            //print(datastring)
            //XML Parser is appearently thread safe
            let xml = NSXMLParser(data: data!)
            xml.delegate = self
            xml.setValue(response!.URL!.resourceSpecifier, forKey: "url")
            xml.parse()
        }
    }

    
    
    
    var currentParserElement = ""
    func parser(parser: NSXMLParser,
        didStartElement elementName: String,
        namespaceURI: String?,
        qualifiedName: String?,
        attributes attributeDict: [String: String]){
            //as parser is thread safe, I should just be able to set the current element name to help identify where the data is coming fomr
            currentParserElement = elementName.replace("Result", with: "")
    }
    var currentElementData = ""
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        print("currentParserElement=\(currentParserElement)")
        print("Encoded proof data as base64 string length=\(currentElementData.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))")
        let data: NSData = currentElementData.dataUsingEncoding(NSUTF8StringEncoding)!
        //var error: NSError?
        
        do {
            if let result = try jsonToDictionaryFromData(data) {
                wsResponseDelegate.response(currentParserElement,data:result)
                currentElementData = ""
            }
            else {
                var result = Dictionary<String, AnyObject> ()
                if let decodedData = NSData(base64EncodedData: data, options: []) {
                    //see if it's a string
                    if let datastring = NSString(data: decodedData, encoding:NSUTF8StringEncoding) {
                        //assume JSON - not right to do this really, but for now...
                        if let result = jsonToDictionaryFromString(datastring as String) {
                            wsResponseDelegate.response(parser.valueForKey("url") as! String, data: result)
                        }
                        currentElementData = ""
                        return
                    }
                    result["fileData"] = decodedData
                    wsResponseDelegate.response(parser.valueForKey("url") as! String, data: result)
                    return
                } else {
                    if let decodedData = NSData(base64EncodedString: currentElementData, options: []) {
                        result["fileData"] = decodedData
                        wsResponseDelegate.response(parser.valueForKey("url") as! String, data: result)
                        currentElementData = ""
                        return
                    } else {
                        //?
                        print("unable to decode proof data")
                        currentElementData = ""
                    }
                }
            }
        }
        catch {
            //ouch
        }
        
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String){
        currentElementData += string
    }
    
    
    
    func parser(parser: NSXMLParser, foundCDATA CDATABlock: NSData) {
        //possibly needed for getFile routines (though I think it's always Base64 Strings)
        print("foundCDATA is \(CDATABlock)")
    }
    
    
    
    func jsonToDictionaryFromString(json:String)-> Dictionary<String,String>? {
        if let data = json.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            do {
                return try jsonToDictionaryFromData(data)
            }
            catch {
                //return nil
            }
        }
        return nil
    }
    
    func jsonToDictionaryFromData(json:NSData)throws -> Dictionary<String,String>? {
        do {
            //print(json)
            let jsonArray:[AnyObject] = try NSJSONSerialization.JSONObjectWithData(json, options: NSJSONReadingOptions.AllowFragments) as! [AnyObject]
            //print(jsonArray)
            return jsonToDictionaryFromArray(jsonArray)
        }
        catch let error as NSError {
            // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
            print("A JSON parsing error occurred, here are the details:\n \(error)")
            return nil
        }
        //return nil
    }
    
    
    func jsonToDictionaryFromArray(json:[AnyObject])-> Dictionary<String,String>? {
        //todo: come up with a better pattern to restrict an instance to a single call and have this as an object-level variable
        //TODO: is this is a generic class, this needs to be generic and not specific to Docmail's JSON formatting
        var result = Dictionary<String,String>()
        for item in json {
            if let key = item["Key"] as! String? {
                if let value: AnyObject = item["Value"] {
                    result[key] = "\(value)"
                }
            }
        }
        return result
    }
    
}


extension String{
    func replace(replace:String, with:String)->String{
        return self.stringByReplacingOccurrencesOfString(replace, withString: with, options: .LiteralSearch, range: nil)
    }
}


class JSONWrapper: NSObject, NSURLConnectionDataDelegate {
    
    var wsUrl = ""
    let connectionCheckAddress = "https://www.cfhdocmail.com"
    var wsResponseDelegate :WebserviceResponse
    
    init(wsResponseDelegate :WebserviceResponse!,wsUrl:String){
        self.wsUrl = wsUrl
        self.wsResponseDelegate = wsResponseDelegate
    }
    
    func fetchURL(address:String){
        Reachability.testNetworkWithHead(networkTimeoutSecs, testURL: connectionCheckAddress) { (headData:NSData?, headResponse:NSURLResponse?, headError:NSError?) -> Void in
            
            //if there was an error show as not connected (not quite correct as we don't pass the error back up the chain...)
            if (headError != nil) {
                self.wsResponseDelegate.notConnected()
                return
            }
            
            let url = NSURL(string: address)!
            
            let webClient=NSURLSession.sharedSession().dataTaskWithURL(url,completionHandler: self.gotResponse)
            webClient.resume()
        }
    }
    
    
    func jsonSend(methodName:String, data:Dictionary<String, String>){
        
        Reachability.testNetworkWithHead(networkTimeoutSecs, testURL: connectionCheckAddress) { (headData:NSData?, headResponse:NSURLResponse?, headError:NSError?) -> Void in
            
            //if there was an error show as not connected (not quite correct as we don't pass the error back up the chain...)
            if (headError != nil) {
                print("error: \(headError)")
                self.wsResponseDelegate.notConnected()
                return
            }
            
            var error:NSError?
            let body: NSData?
            do {
                body = try NSJSONSerialization.dataWithJSONObject(data, options: [])
            } catch let error1 as NSError {
                error = error1
                body = nil
            } catch {
                fatalError()
            }
            if error != nil{
                //TODO: learn error handling in Swift!
                print("error: \(error)")
            }
            
            let url = NSURL(string:"\(self.wsUrl)/\(methodName)")!
            let msgLength = String(body!.length)
            let request = NSMutableURLRequest(URL: url)
            
            request.addValue("application/json; charset=utf-8",  forHTTPHeaderField: "Content-Type")
            request.addValue("application/json",                 forHTTPHeaderField: "Accept")
            request.addValue(msgLength,                          forHTTPHeaderField: "Content-Length")
            request.addValue(self.wsUrl,                         forHTTPHeaderField: "url")
            request.addValue(methodName,                         forHTTPHeaderField: "method")
            
            request.HTTPMethod = "POST"
            request.HTTPBody = body
            //init and run HTTP reque
        
            //var conn = NSURLConnection(request:request,delegate:self,startImmediately:true)
            
            let webClient=NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: self.gotResponse)
            webClient.resume()
        }
    }
    
    func gotResponse(data:NSData?, response:NSURLResponse?, error:NSError?){
        if error != nil{
            print("error: \(error)")
            wsResponseDelegate.error(data, response:response, error:error)
        }
        else {
            var notJson = false
            var didUtf8Decode = false
            if let datastring = NSString(data: data!, encoding: NSUTF8StringEncoding) {
                didUtf8Decode = true
                if let jsonResult = try!NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? Dictionary<String, AnyObject> {
                    if error != nil{
                        //TODO: learn error handling in Swift!
                        print("error: \(error)")
                    }
                    print(datastring)
                    print(jsonResult)
                    
                    let result = jsonResultProcessor(jsonResult)
                    
                    //TODO: Handle Docmail error/validation responses correctly
                    print(result)
                    wsResponseDelegate.response(response!.URL!.resourceSpecifier, data: result)
                    return
                }
                else{
                    notJson = true
                    //assume textual file, not Base64 encoded binary
                    
                    var result = Dictionary<String, AnyObject> ()
                    result["textData"] = datastring
                    print(result)
                    wsResponseDelegate.response(response!.URL!.resourceSpecifier, data: result)
                    return
                }
            }
            else {
                notJson = true
            }
            
            if notJson {  //handle non JSON responses (in the event of HTML being returned for example, or a Docmail proof PDF)

                if !didUtf8Decode {
                    if response!.URL?.lastPathComponent?.rangeOfString("") != nil {
                        //if the URL said file in it, assume Base64 file
                        var result = Dictionary<String, AnyObject> ()
                        
                        let decodedData = NSData(base64EncodedData: data!, options: NSDataBase64DecodingOptions(rawValue: 0))
                        
                        result["fileData"] = decodedData
                        print(result)
                        wsResponseDelegate.response(response!.URL!.resourceSpecifier, data: result)
                        return
                    }
                    else {
                        //not sure what this is - probably non UTF-8 compatible text!
                    }
                }
            }
        }
    }
    
    func jsonResultProcessor(json:Dictionary<String, AnyObject>) -> Dictionary<String, AnyObject> {
        //default behaviour is to do nothing - can be overidden for things like JSON d:
        return json
    }
    
    func connection(connection:NSURLConnection, didSendBodyData bytesWritten: Int, totalBytesWritten : Int, totalBytesExpectedToWrite: Int){
        wsResponseDelegate.progressUp(connection, bytesWritten: bytesWritten, totalBytesWritten : totalBytesWritten, totalBytesExpectedToWrite: totalBytesExpectedToWrite)
    }
    
    
}
