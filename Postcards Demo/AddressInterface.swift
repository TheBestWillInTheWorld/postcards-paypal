//
//  AddressModel.swift
//
//  Created by Will Gunby on 25/04/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class AddressInterface<T:Address>:ModelInterface<T> {
    
    
    init() {
        super.init(entityName: "Address", matchKey: "Name")
    }
    
    func addAddress(name:String, addressLines:String)->Address{
        let newAddress:Address = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context!) as! Address
        newAddress.name=name
        newAddress.addressLines=addressLines
        newAddress.created=NSDate()
        save()
        return newAddress
    }
    
    
}
