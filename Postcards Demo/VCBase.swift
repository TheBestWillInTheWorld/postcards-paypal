//
//  Alert.swift
//  Fiddling With Postcards
//
//  Created by Will Gunby on 18/04/2015.
//  Copyright (c) 2015 Will Gunby. All rights reserved.
//

import UIKit

public class VCBase:UIViewController {
    
    
    func alert(title:String, message:String){
        alert(title, message: message, buttonText: "OK")
    }
    
    func alert(title:String, message:String, buttonText: String){
        alert(self, title: title, message: message, buttonText:buttonText) { (action:UIAlertAction!) -> Void in    }
    }
    func alert(title:String, message:String, buttonText: String, closure:(UIAlertAction!) -> Void ){
        alert(self, title: title, message: message, buttonText: buttonText, closure: closure)
    }
    func alert(containerViewController:UIViewController, title:String, message:String, buttonText: String, closure:(UIAlertAction!) -> Void ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)

        if buttonText != "" {
            let action = UIAlertAction(title: buttonText, style: UIAlertActionStyle.Default, handler: { action in
                alert.dismissViewControllerAnimated(true, completion: { () -> Void in
                    closure(action)
                })
            })
            alert.addAction(action)
        }

        alert.view.tintColor = appThemeColour
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    var spinny = UIActivityIndicatorView(frame: CGRectMake(0, 0, 200, 200))
    func beginWait(){
        spinny.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 110)
        spinny.hidesWhenStopped = true
        spinny.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        //spinny.color = UIColor(red: 0, green: 122, blue: 255)
        spinny.color = appThemeColour
        self.view.addSubview(spinny)
        spinny.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func endWait(){
        dispatch_async(dispatch_get_main_queue()) {
            self.spinny.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}