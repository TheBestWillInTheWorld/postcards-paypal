//
//  DocmailAPIWrapper.swift
//  Fiddling With Postcards
//
//  Created by Will Gunby on 28/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//
import Foundation


class DocmailAPIWrapper: SOAPWrapper {
    //generate SOAP envelope templates for each method here http://www.soapclient.com/soapclient
    //https://www.cfhdocmail.com/TestAPI2/DMWS.asmx
    
    var username    = ""
    var password    = ""
    
    init(username:String, password:String, wsResponseDelegate :WebserviceResponse!, useLive:Bool){
        self.username = username
        self.password = password
        
        var liveOrTest:String
        if useLive  { liveOrTest = "Live" }
        else        { liveOrTest = "Test" }
        
        let url = "https://www.cfhdocmail.com/\(liveOrTest)API2/DMWS.asmx"        
        super.init(wsResponseDelegate: wsResponseDelegate, wsUrl: url, wsNameSpace: "https://www.cfhdocmail.com/LiveAutoAPI/")
    }
    
    func getBalance(accountType:String?,returnFormat:String){
        let methodName = "GetBalance"
        let fields : Dictionary<String,AnyObject?> = [  "Username"      : username,
                                                        "Password"      : password,
                                                        "AccountType"	: accountType,
                                                        "ReturnFormat"  : returnFormat ]
        
        let content =  contentForMethod(methodName,fieldValues:fields)
        soapSend(methodName, content:content)
    }
    
    func createMailing(customerApplication:String, productType:String, mailingName:String, mailingDesc:String, isMono:Bool, isDuplex:Bool, deliveryType:String, courierDeliveryToSelf:String, despatchASAP:Bool, despatchDate:NSDate, addressNamePrefix:String, addressNameFormat:String, discountCode:String, minEnvelopeSize:String, returnFormat:String){
        let methodName = "CreateMailing"
        let fields : Dictionary<String,AnyObject?> = [  "Username"              : username,
                                                        "Password"              : password,
                                                        "CustomerApplication"   : customerApplication,
                                                        "ProductType"           : productType,
                                                        "MailingName"           : mailingName,
                                                        "MailingDesc"           : mailingDesc,
                                                        "IsMono"                : isMono,
                                                        "IsDuplex"              : isDuplex,
                                                        "DeliveryType"          : deliveryType,
                                                        "CourierDeliveryToSelf" : courierDeliveryToSelf,
                                                        "DespatchASAP"          : despatchASAP,
                                                        "DespatchDate"          : despatchDate,
                                                        "AddressNamePrefix"     : addressNamePrefix,
                                                        "AddressNameFormat"     : addressNameFormat,
                                                        "DiscountCode"          : discountCode,
                                                        "MinEnvelopeSize"       : minEnvelopeSize,
                                                        "ReturnFormat"          : returnFormat ]
    
        let content =  contentForMethod(methodName,fieldValues:fields)
        soapSend(methodName, content:content)
    }
    
    func getProofFile(mailingGuid:String, failureReturnFormat:String){
        let methodName = "GetProofFile"
        let fields : Dictionary<String,AnyObject?> = [  "Username"              : username,
            "Password"              : password,
            "MailingGUID"           : mailingGuid,
            "FailureReturnFormat"   : failureReturnFormat ]
        
        let content =  contentForMethod(methodName,fieldValues:fields)
        print(content)
        soapSend(methodName, content:content)
    }
    
    
}
